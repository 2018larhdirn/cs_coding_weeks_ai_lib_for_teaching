# Projet *Contribuez à l'Open Source*


## Petit bilan sur ce qui a été fait le JOUR 1

Lors de la première journée vous avez :

 + Pris en main le système de partage de versions `git` 
 + Developper un jeu 2048 en mode console en suivant l'approche Test Driven Developpement.
 + Vos projets doivent être pour une grande majorité sur le Gitlab et vous pourriez alors rendre ce projet public en changeant ses permissions et en le rendant `Public`.
 
 ![gitlabpermissions](./Images/gitlabpublic.png)


Votre projet deviendrait alors un projet [open-source](https://fr.wikipedia.org/wiki/Open_source). Enfin, pas complétement...

**Prenez le temps de lire ce très bon [tutoriel](https://opensource.guide/fr/starting-a-project/) qui explique ce qu'est l'open-source !** C'est votre premier travail de la journée.


 Alors que manque t'il à votre projet pour le rendre open source. Si on reprend le tutorial, il faut, en plus du code lui-même :
 
 + [Une Licence open source](https://help.github.com/articles/licensing-a-repository/).
 + Un README (que vous avez peut-être).
 + Des Lignes directrices contributives. (un fichier `CONTRIBUTING.md`)
 + Un Code de conduite, par exemple en regardant [cela](https://www.contributor-covenant.org/).


Nous allons maintenant travailler sur deux objectifs :

1. **Contribuer à l'Open Source**, en participant à la résolution d'issues sur des projets open-source existants.

2. **Creer un projet Open-Source** pour le développement d'une bibliothèque Open-Source pour l'enseignement de l'IA. Il s'agira ici de réutiliser ce que vous avez commencer à faire autour du jeu 2048 mais en ajoutant une brique d'intelligence artificielle.


Commencons par [contribuer à l'Open Source!](./contribuer.md)


 
 
 
 






