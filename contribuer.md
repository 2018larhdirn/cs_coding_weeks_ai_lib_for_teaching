# Objectif 1 : contribuons !

L'objectif ici est de contribuer à plusieurs projets open-source. A nouveau, vous pouvez lire ce très bon [guide pour faire des contributions open source](https://opensource.guide/fr/how-to-contribute/).


## Sprint 1 : une première contribution pour apprendre à faire des contributions.

Pour commencer, nous allons faire ce mini [tutorial](https://github.com/firstcontributions/first-contributions).

Attention, ici nous allons utiliser [Github](https://github.com) et non plus GitLab et il sera peut être nécessaire de vous inscrire sur GitLab. GitHub est la plate-forme la plus répandue pour les projets open-source.

Il faut donc suivre le tutorial et donc :

 + Faire un `fork` (une copie sur le serveur) du projet [first-contributions](https://github.com/firstcontributions/first-contributions).
 + Cloner en local le fork du projet avec la commande `git clone`
 + Créer une branche avec la commande `git checkout -b`
 + Changer le fichier `Contributors.md` en ajoutant votre nom.
 + Puis faire le cycle :
 	+ `git status` pour voir le statut de votre dépôt
 	+ `git add Contributors.md`
 	+ `git commit -m " Add your-name to Contributors list `
 	+ `git push origin your-branch`

 + Et enfin, il faudra soumettre votre code, vos changements pour une review.

 ![gitpush](./Images/gitpush.png)
  		

## Sprint 2 : Une première issue.

La contribution à l'opensource consiste souvent à fixer des `issues` pour le projet. Vous pouvez y acceder depuis GitLab ou GitHub. Certains, pour le projet `2048` ont peut-être eu des issues qui ont été ajoutées à leur projet par l'équipe des coding weeks.

![issues](./Images/issue.png)

Vous allez donc travailler de la manière suivante.

 + Chaque binôme va relire le code d'un autre binôme et **ajouter une issue** (il y en a très certainement, comme par exemple des problèmes de nommage ou le manque de commentaires). Il faudra donner les droits de `Developper` aux membres respectifs. Un autre binôme sera chargé  de proposer une solution pour résoudre l'issue, en appliquant le principe précédent.

 

## Sprint 3 : Un peu plus d'issues

Il existe un certain nombre de plate-formes pour permettre de rechercher et même d'être notifier d'issues ouvertes dans des projets open-source. Quelques unes sont listées ci-dessous :

 + [GitHub Explore](https://github.com/explore)
 + [First Timers Only](https://www.firsttimersonly.com/)
 + [Your First PR](https://yourfirstpr.github.io/)
 + [CodeTriage](https://www.codetriage.com/)
 + [24 Pull Requests](https://24pullrequests.com/)
 + [Contributor-ninja]()
 + [issuehub](http://issuehub.io/)
 + [up-for-grabs](https://up-for-grabs.net/#/)
 
 Elles peuvent aussi être utilisées pour trouver des `issues` faciles. Nous proposons d'utiliser la plate forme `CodeTriage` pour vous permettre de choisir votre langage et d'identifier des projets auxquels contribuer mais d'autres sont possibles. 
 
 Certains projets ont un label `good first issue` ou equivalent que vous pouvez utiliser pour chercher des issues faciles. Vous pouvez par exemple trouver des projets avec des `issues` et ce label avec [http://issuehub.io/](http://issuehub.io/?label%5B%5D=good+first+issue&language=python)
 
 
 C'est par exemple le cas de [scikit-image](https://github.com/scikit-image/scikit-image/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22).
 
 
 ![scikit](./Images/scikit.png)
 
 
 
 L'objectif de cette journée est de résoudre au mieux un maximum d'issues sur des projets python. Vous pourrez faire votre choix via codetriage.
Vous pouvez par exemple.
 
 
 Vous pouvez passer maintenant à l'[Objectif 2 : Créer un projet OpenSource](./opensourceproject.md).
 
 
 
 
 
